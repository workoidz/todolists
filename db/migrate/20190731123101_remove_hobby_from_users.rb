class RemoveHobbyFromUsers < ActiveRecord::Migration[6.0]
  def change

    remove_column :users, :hobby, :string
  end
end
